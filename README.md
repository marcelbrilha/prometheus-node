# Prometheus - Node

Rodar Prometheus Docker:

```sh
docker run --name prometheus --rm -d -p 9090:9090 -v ~/Documentos/dev/prometheus-node/prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus
```

Rodar Grafana Docker:

```sh
docker run -d --name=grafana -p 3000:3000 grafana/grafana
```

Dev - Porta 3001:

```sh
npm run dev
```

Start - Porta 3001:

```sh
npm run start
```
